#jcalendarchooser
<p>
      Java Swing 日期,日期时间选择组件
</p>

<p>
      使用要求: JDK 1.6 +
</p>
<hr/>
<div>
    <h3>截图</h3>
    <ul>
        <li>
            <strong>JCalendarChooser</strong>
            <br/>
            <img src="http://shengzhao.qiniudn.com/JCalendarChooser.jpg" alt="JCalendarChooser.jpg"/>
        </li>
        <li>
          <strong>JTimeChooser</strong>
          <br/>
          <img src="http://shengzhao.qiniudn.com/JTimeChooser.jpg" alt="JTimeChooser.jpg"/>
        </li>
    </ul>
</div>

<hr/>
<h3>说明与使用</h3>


JCalendarChooser   封装的日期选择器

使用:
<br/>
    1.创建JCalendarChooser对象(需要参数)
    <br/>
    2.调用showCalendarDialog()方法获取选择的日期对象(Calendar)


       李胜钊   2010-07-11 13:31



JTimeChooser     封装对时间的选择器

使用:
<br/>
    1.创建JTimeChooser     对象(需要参数)
    <br/>
    2.调用showTimeDialog()方法获取选择的日期对象(Calendar)



       李胜钊   2010-08-05 23:27


   2010-08-26 修改默认打开后点击确定返回当前日期值
   <br/>
   2010-09-03 修改在不同观感下显示不正确与设置显示年份数的问题

<hr/>
<p>
    <h3>下载</h3>
    <a href="http://download.csdn.net/detail/monkeyking1987/2534062">CSDN资源下载jar</a>
    <br/>
    <a href="http://git.oschina.net/mkk/jcalendarchooser/raw/master/jar/calendar.jar">直接下载jar</a>
    <br/>
    <br/>
    欢迎一起讨论,一起改进该小工具程序
    <br/>
    我的联系方式: <a href="mailto:monkeyk1987@gmail.com">monkeyk1987@gmail.com</a>
    <br/>
    关注更多我的开源项目请访问 <a href="http://andaily.com/my_projects.html">http://andaily.com/my_projects.html</a>
</p>